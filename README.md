## About

The idea is to recopile knowledge about DTW concepts, and scripts about its applications.

Identify in which cases in can be good/bad to apply.

---
## Examples

* [test_dtw.py](./test_dtw.py): compares two time-series, where one of theme is a sinusoidal, and the other also a sinusoidal but with a superposed noise and with a longer wavelength associated.
  Both of them complete one cycle.

![some textt](images/test_dtw.png "example 1")


* [test_dtw2.py](./test_dtw2.py): compares two time-series, whete one of them is a simple sinusoidal, and other too but with more resolution, a longer wavelength, and superposed noise.
  This validates that our DTW implementation supports the comparison of two time-series that have different wavelength.

![some textt](images/test_dtw2.png "example 2")


<!--- EOF -->
