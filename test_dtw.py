#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import euclidean

import dtw as dtw_funcs

plt.style.use("dark_background")



start = 0
end   = 2*np.pi
step  = 0.1
k     = 1.5

x1 = np.arange(start, end, 1.0*k*step)
x2 = np.arange(start, end/k, step)

noise = np.random.uniform(0, 2*np.pi, x1.size) / 10

y1 = np.sin(x1) + 1*np.sin(2*x1) + noise
y2 = np.sin(k*x2) + 1*np.sin(k*2*x2)
#y1[11:15] += 15.
#x = np.array([1, 1, 2, 3, 2, 0])
#y = np.array([0, 1, 1, 2, 3, 2, 1])

ax1 = plt.plot(x1, y1, '-o')
plt.setp(ax1, color='b', linewidth=1.0, ms=2, label='N:%d' % y1.size)

ax2 = plt.plot(x2, y2, '-o')
plt.setp(ax2, color='r', linewidth=1.0, ms=2, label='N:%d' % y2.size)


path, cost, dist = dtw_funcs.dtw(y1, y2, metric=euclidean)

for [mx1, mx2] in path:

    #print(mx1, x1[mx1], ':', mx2, x2[mx2])

    plt.plot([x1[mx1], x2[mx2]], [y1[mx1], y2[mx2]], 'g', alpha=0.3)

plt.legend(loc='best')
plt.show()
